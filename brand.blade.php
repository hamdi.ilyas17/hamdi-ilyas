@extends('layouts.app')

@section('content')		
<title>Coba</title>

<div class="container py-3">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Master Data</a></li>
				<li class="breadcrumb-item active" aria-current="page">Edit Brand</li>
			</ol>
	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="mb-10">
						<label for="exampleFormControlInput1" class="form-label">Nama Brand</label>
						<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Ketik disini...">
					</div>
					<div>
						<a href="#" class="btn btn-primary mt-4">Simpan Brand</a>
					</div>
				</div>
			</div>
		</div>
			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
						<div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th scope="col">Kode Brand</th>
										<th scope="col">Nama Brand</th>
										<th scope="col">Options</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>BRND/20220403/001</td>
										<td>Kawasaki</td>
										<td>
											<a href="#" class="btn btn-warning">Edit Brand</a> &nbsp; 
											<a href="#" class="btn btn-danger">Hapus</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
	
	
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>